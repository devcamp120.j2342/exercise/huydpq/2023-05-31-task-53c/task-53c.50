import interfaces.Movable;
import models.MovableCircle;
import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point = new MovablePoint(10, 15, 4, 7);
        MovableCircle circle = new MovableCircle(5, point);
        System.out.println(point.toString());
        System.out.println(circle.toString());
        System.out.println("--------------------------------");

        point.moveUp();
        point.moveUp();
        point.moveLeft();
        System.out.println(point.toString());

        circle.moveRight();
        circle.moveDown();
        System.out.println(circle.toString());

    }
}
