package models;
import interfaces.Movable;

public class MovableCircle implements Movable {
    private double radius;
    private MovablePoint center;


    public MovableCircle(double radius, MovablePoint center) {
        this.radius = radius;
        this.center = center;
    }

    @Override
    public String toString() {
        return "MovableCircle [" + center + ", radius=" + radius + "]";
    }
    
    @Override
    public void moveUp() {
        center.moveUp();
    }

    @Override
    public void moveDown() {
        center.moveDown();
    }

    @Override
    public void moveLeft() {
        center.moveLeft();
    }

    @Override
    public void moveRight() {
        center.moveRight();
    }


    
}
